package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    boolean keepPlaying = true;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

    public void run() {
        // TODO: Implement Rock Paper Scissors
        while (keepPlaying){
            // UI, start
            System.out.println(String.format("Let's play round %d", roundCounter));

            // User choice
            String userChoice = userChoice();

            // Computer choice
            String computerChoice = computerChoice(rpsChoices);

            // Checking who won the round
            whoWon(userChoice, computerChoice);

            // Asking user to keep playing
            keepPlaying();
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

    // User choice method
    public String userChoice(){
        // Checking if user input is valid
        while (true) {
            String userChoice = readInput("Your choice (Rock/Paper/Scissors)?");
            if (!userChoice.equals("rock") && !userChoice.equals("paper") && !userChoice.equals("scissors")) {
                System.out.println(String.format("I do not understand %s. Could you try again?", userChoice));
            }
            else {
                return userChoice;
            }
        }
    }

    // Computer choice method
    public String computerChoice(List<String> choices) {
        int randomNumber = (int)(Math.random() * 3);
        String computerChoice = choices.get(randomNumber);
        return computerChoice;
    }

    // Checking who won the round
    public void whoWon(String userChoice, String computerChoice) {
        if (userChoice.equals(computerChoice)) {
            System.out.println(String.format("Human chose %s, computer chose %s. It's a tie!", userChoice, computerChoice));
        }
        else if (userChoice.equals("rock") && computerChoice.equals("scissors") || userChoice.equals("paper") && computerChoice.equals("rock") || userChoice.equals("scissors") && computerChoice.equals("paper")) {
            humanScore++;
            System.out.println(String.format("Human chose %s, computer chose %s. Human wins!", userChoice, computerChoice));
        }
        else {
            computerScore++;
            System.out.println(String.format("Human chose %s, computer chose %s. Computer wins!", userChoice, computerChoice));
        }
        System.out.println(String.format("Score: human %d, computer %d", humanScore, computerScore));
    }

    // Keep playing method
    public void keepPlaying(){
        while (true) {
            String userInput = readInput("Do you wish to continue playing? (y/n)?");
            if (userInput.equals("y")) {
                roundCounter++;
                keepPlaying = true;
                break;
            }
            else if (userInput.equals("n")) {
                System.out.println("Bye bye :)");
                keepPlaying = false;
                break;
            }
            else {
                System.out.println(String.format("I do not understand %s. Could you try again?", keepPlaying));
            }
        }
    }
}